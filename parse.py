#!/usr/bin/python3

import sys
import argparse
import json

obj_types = {
    3: "alert",
    4: "alerts",
    5: "library",
    6: "libraries",
    7: "block",
    8: "blocks",
    9: "frame",
   10: "frames",
   11: "canvas",
   12: "canvases",
   13: "lov_item",
   14: "lov_items",
   15: "rg_item",
   16: "rg_items",
   19: "globals",
   22: "module",
   24: "parameter",
   25: "parameters",
   26: "record_source_column",
   27: "record_source_columns",
   30: "item",
   31: "items",
   34: "combobox_item",
   35: "combobox_items",
   36: "lov",
   37: "lovs",
   40: "menu",
   41: "menus",
   42: "menuitem",
   43: "menuitems",
   58: "procedure",
   59: "procedures",
   64: "record_group",
   65: "record_groups",
   66: "relation",
   67: "relations",
   68: "trigger",
   69: "triggers",
   75: "vis_attr",
   76: "vis_attrs",
   78: "window",
   79: "windows",
   93: "refs",
   94: "ref",
  100: "binding",
  101: "bindings",
}
prop_names= {
    6: "button0_label",
    7: "button1_label",
    8: "button2_label",
    9: "default_button",
   10: "alert_message",
   11: "alert_type",
   19: "background_color",
   33: "canvas",
   46: "sync_with_item",
   59: "console_window",
   67: "label_foreground_color",
   95: "fixed_length",
   97: "menu_module",
  103: "font_name",
  104: "font_size",
  108: "foreground_color",
  110: "format_mask",
  121: "height",
  124: "item_hint_text",
  127: "as_icon",
  129: "icon_name",
  136: "item_refs_list",
  137: "item_refs_obj",
  139: "item_type", 
  141: "data_type",
  151: "item_label",
  156: "library_path",
  157: "library_source_type",
  159: "combobox_items_obj",
  165: "lov_name",
  168: "check_by_lov",
  175: "max_length",
  177: "hor_toolbar_canvas",
  211: "name",
  220: "next_nav_item",
  241: "sql_order_by",
  268: "records_to_display",
  270: "records_space",
  278: "cur_rec_vis_attribute",
  283: "required",
  288: "label_font_name",
  290: "scrollbar_canvas",
  293: "scrollbar_width",
  294: "scrollbar_left",
  295: "scrollbar_top",
  307: "title",
  311: "label_font_size",
  312: "trigger_refs_list",
  314: "trigger_refs_obj",
  327: "item_value",
  328: "visual_attribute",
  344: "column_name",
  359: "sql_where",
  361: "width",
  366: "title_strref",
  372: "left",
  373: "top",
  380: "item_hint_show_auto",
  383: "mouse_hint_text",
  394: "label_text",
  399: "label_anchor_shift",
  401: "label_align_shift",
  464: "plsql_text",
  465: "length",
#  517: "item_refs",
  480: "items_to_display",
  498: "tab_page",
  515: "popup_menu",
  518: "recourd_source_column_refs",
  522: "record_source_name",
  523: "column_name",
  607: "mandatory",
  730: "button0_label_strref",
  731: "button1_label_strref",
  732: "button2_label_strref",
  733: "message_strref",
  762: "scrollbar_length",
  766: "formula",
}


DEF_FRM_IDFO = "DEFINE  FRM50_IDFO"
DEF_F50T = "DEFINE  F50T"
DEF_F50P = "DEFINE  F50P"

DEF_IDFOS_POI = "IDFOS_POI"
DEF_IDFOS_OID = "IDFOS_OID"
DEF_IDFOS_NAM = "IDFOS_NAM"
DEF_IDFOS_TYP = "IDFOS_TYP"
DEF_IDFOS_OOI = "IDFOS_OOI"
DEF_IDFOS_OPN = "IDFOS_OPN"
DEF_IDFOS_CNT = "IDFOS_CNT"

class FormObject:
  def __init__(self, params):
    self.id = int(params['IDFOS_OID'])
    self.parent_id = int(params['IDFOS_POI'])
    self.name = params['IDFOS_NAM']
    self.o_type = int(params['IDFOS_TYP'])
    self.ooi = int(params['IDFOS_OOI'])
    self.opn = int(params['IDFOS_OPN'])
    self.cnt = int(params['IDFOS_CNT'])
    self.procs = {}
    self.props = {}
  def __repr__(self):
    return "%d=(PL/SQL)" % (self.id)
  def get_name(self):
    if self.o_type in obj_types:
      return self.name +"(%s,%d)" % (obj_types[self.o_type], self.id)
    else:
      return self.name +"(%d,%d)" % (self.o_type, self.id)
  def get_short_name(self):
    if self.name == "":
      return ""
    if self.name == "\"\"":
      return ""
    if self.name[0] == "\"" and self.name[-1]=="\"":
      return self.name[1:-1]
    return self.name

  def merge_props(self):
    m = {}
    for idx in self.props:
      p = self.props[idx]
      t = p.n
      if int(t) in prop_names:
        t = prop_names[int(t)]
      if t not in m:
        m[t] = []
      v = p.value.__repr__() 
      if v.startswith('\"') and v.endswith('\"'):
        v = v[1:-1]
      if v.startswith('\'') and v.endswith('\''):
        v = v[1:-1]
      if v.startswith('\"') and v.endswith('\"'):
        v = v[1:-1]
      m[t].append(v)
    for t in m:
      if len(m[t]) == 1:
        m[t] = m[t][0]
    return m

class FormProc:
  def __init__(self, params):
    self.id = int(params['PI'])
    self.parent_id = int(params['PP'])
    self.n = int(params['PN'])
    self.length = int(params['PL'])
    self.value = convert_blong(params['PV'])
    if (not self.value.success):
      self.value.dump()
#    if not self.value[2]:
#      print (params)
#      dump(blong_to_bts(params['PV']), self.value[3])
  def __repr__(self):
    return "%d(P)=(PL/SQL)" % (self.id)

class FormT:
  def __init__(self, params):
    self.id = int(params['TI'])
    self.parent_id = int(params['TP'])
    self.n = int(params['TN'])
    self.value = params['TV']
    if self.value == "NULLP":
      self.value = ""
  def __repr__(self):
    return "%d(T)=%s" % (self.n, self.value)

class FormN:
  def __init__(self, params):
    self.id = int(params['NI'])
    self.parent_id = int(params['NP'])
    self.n = int(params['NN'])
    self.value = int(params['NV'])
  def __repr__(self):
    return "%d(N)=%d" % (self.n, self.value)

class FormB:
  def __init__(self, params):
    try:
      self.id = int(params['BI'])
    except KeyError:
      print(params)
    self.parent_id = int(params['BP'])
    self.n = int(params['BN'])
    self.value = int(params['BV'])
  def __repr__(self):
    return "%d(B)=%d" % (self.n, self.value)

class FormO:
  def __init__(self, params):
    self.id = int(params['OI'])
    self.parent_id = int(params['OP'])
    self.n = int(params['ON'])
    self.value = int(params['OO'])
  def __repr__(self):
    return "%d(O)=%d" % (self.n, self.value)

class BlongInfo:
  FLAG_TRIGGER   = 0x80
  FLAG_PROCEDURE = 0x90
  FLAG_NONE      = 0x00

  def __init__(self):
    self.source = ""
    self.uid = ""
    self.bts = bytes()
    self.success = True
    # 0x42
    self.id1 = 0
    # 0x80 - trigger, 0x90 - procedure
    self.flag0 = 0
    self.buf1 = []
    self.len1 = 0
    # 0x31
    self.id2 = 0
    # 0x0000
    self.ignore1 = 0
    # 0x0044
    self.ignore2 = 0
    # 0x0000
    self.ignore3 = 0
    # 0x0000000a
    self.ignore4 = 0
    self.flag1 = 0
    self.flag2 = 0
    self.flag3 = 0
    self.len3 = 0
    # 0x00ab
    self.id3 = 0
    self.bs = ""
    self.cs = ""
    self.es = ""
    self.ns = ""
  def __repr__(self):
    return "%s" % (self.uid)
  def print_info(self):
    print ("%08x BTS" % (len(self.bts)))
    print ("%08x ID1" % (self.id1))
    print ("%08x FLAG0" % (self.flag0))
    print ("%08x LEN1" % (self.len1))
    print ("%08x FLAG1" % (self.flag1))
    print ("%08x FLAG2" % (self.flag2))
    print ("%08x FLAG3" % (self.flag3))
    print ("%08x LEN3" % (self.len3))
    print ("%08x BS" % (len(self.bs)))
    print ("%08x CS" % (len(self.cs)))
    print ("%08x ES" % (len(self.es)))
    print ("%08x NS" % (len(self.ns)))
    
  def dump(self, pos = 0):
    print ("=== %04x" % pos)
    s = ["%02x"%(x) for x in self.bts]
    while (len(s) % 32) != 0:
      s.append("")

    shift = 0
    s0 = []
    while (shift < len(s) - 4):
      s0.append("".join(s[shift:shift+4]))
      shift = shift + 4

    shift = 0
    while (shift < len(s0) - 8):
      ss = " ".join(s0[shift:shift+8])
      print ("%04x %s" % (shift*4,ss))
      shift = shift + 8

    ss = " ".join(s0[shift:])
    print ("%04x %s" % (shift*4,ss))
    shift = shift + 8
  def print_code(self, f):
    if (self.flag0 == self.FLAG_TRIGGER):
#      pass
      self.print_trigger(f)
    elif (self.flag0 == self.FLAG_PROCEDURE):
      self.print_procedure(f)
    elif (self.flag0 == self.FLAG_NONE):
      pass
    else:
      print("%08x %s" % (self.flag0, self.uid), file = f)
  def print_trigger(self, f):
    print ("-- ", file=f)
    print ("-- %s" % self.ns, file=f)
    print ("-- ", file=f)
    print("PROCEDURE %s IS" % self.uid, file=f)
    print ("BEGIN", file=f)
    lines = self.cs.split('\n')
    for line in lines:
      print (line, file=f)
    print ("END %s;" % self.uid, file=f)
  def print_procedure(self, f):
    print ("-- %s" % self.uid, file=f)
    lines = self.cs.split('\n')
    for line in lines:
      print (line, file=f)

def parse_string(bts,pos):
  (bss,bsl,pos) = parse_bytes(bts, pos, 1)
  if (bsl < 0):
    return ("", bsl, pos)
  try:
    ss = bss.decode("windows-1251")
    return (ss, bsl, pos)
  except UnicodeDecodeError:
    print(bss)
    return ("<DECODE_ERROR>", bsl, pos)

def parse_uint32(bts, pos):
  id1 = 0
  id1 = id1 * 256 + bts[pos+0]
  id1 = id1 * 256 + bts[pos+1]
  id1 = id1 * 256 + bts[pos+2]
  id1 = id1 * 256 + bts[pos+3]
  pos = pos + 4

  return (id1,pos)

def parse_uint16(bts, pos):
  id1 = 0
  id1 = id1 * 256 + bts[pos+0]
  id1 = id1 * 256 + bts[pos+1]
  pos = pos + 2

  return (id1,pos)

def parse_bytes(bts, pos, chunk_size = 4):
  if (pos > len(bts) - 4):
    return ([], -1, pos)

  (len1,pos) = parse_uint32(bts, pos)
  if (pos > len(bts) - chunk_size * len1):
    return ([], -2, pos)

  buf = bts[pos:pos + len1 * chunk_size]
  pos = pos + len1 * chunk_size
  return (buf,len1, pos)

def blong_to_bts(s):
  if s.startswith('"'):
    s = s[1:]
  if s.endswith('"'):
    s = s[:-1]
  s=s.replace(" ","")
  bts = bytes.fromhex(s)
  return bts

def convert_blong(s):

  if s.endswith("NULLP"):
    return BlongInfo()
  binfo = BlongInfo()

  bts = blong_to_bts(s)

  binfo.bts = bts

  pos = 0
  (binfo.id1,pos) = parse_uint32(bts, pos)
  (binfo.flag0,pos) = parse_uint16(bts, pos)
  (binfo.ignore1,pos) = parse_uint16(bts, pos)
  (binfo.ignore2,pos) = parse_uint16(bts, pos)
  (binfo.ignore3,pos) = parse_uint16(bts, pos)
  (binfo.buf1, binfo.len1, pos) = parse_bytes(binfo.bts, pos)
  if binfo.len1 < 0:
    binfo.success = False
    binfo.dump()
    return binfo
  (binfo.id2,pos) = parse_uint32(bts, pos)
  if binfo.id2 != 0x31:
    binfo.success = False
    binfo.dump()
    return binfo
  (binfo.ignore4,pos) = parse_uint32(bts, pos)
  (binfo.flag1,pos) = parse_uint32(bts, pos)
  (binfo.flag2,pos) = parse_uint32(bts, pos)
  (binfo.flag3,pos) = parse_uint32(bts, pos)

  (binfo.len3,pos) = parse_uint32(bts, pos)
  (binfo.id3,pos) = parse_uint16(bts, pos)

  if binfo.id3 != 0x00ab:
    binfo.success = False
    binfo.dump()
    return binfo

  (binfo.uid, l, pos) = parse_string(bts, pos)
  if (l < 0):
    binfo.success = False
    binfo.dump()
    return binfo

  if binfo.flag0 == binfo.FLAG_TRIGGER:
    (binfo.bs, bl, pos) = parse_string(bts, pos)
    (binfo.cs, cl, pos) = parse_string(bts, pos)
    (binfo.es, el, pos) = parse_string(bts, pos)
    (binfo.ns, nl, pos) = parse_string(bts, pos)
    if binfo.bs != "BEGIN\n" or binfo.es != "\nEND;":
      print ("ERR BEGIN-END %08x/%08x/%s/%s/%s/" % (binfo.flag1, binfo.flag3,binfo.bs,binfo.cs,binfo.es))
      binfo.success = False
      binfo.dump()
      return binfo
  else:
    (binfo.cs, cl, pos) = parse_string(bts, pos)
    if (cl < 0):
      print ("ERR BS %08x/%08x/%s/%04x/%s/" % (binfo.flag1, binfo.flag3,binfo.uid,pos,binfo.cs))
      binfo.success = False
      binfo.dump()
      return binfo
    elif (cl == 0):
      (binfo.cs, cl, pos) = parse_string(bts, pos)
    if (cl < 0):
      print ("ERR CS %08x/%08x/%s/%04x/%s/" % (binfo.flag1, binfo.flag3,binfo.uid,pos,binfo.cs))
      binfo.success = False
      binfo.dump()
      return binfo
    if (binfo.cs == "<DECODE_ERROR>"):
      print ("ERR CS %08x/%08x/%s/%04x/%s/" % (binfo.flag1, binfo.flag3,binfo.uid,pos,binfo.cs))
      binfo.print_info()
      binfo.success = False
      binfo.dump()
      return binfo
#    (binfo.es, el, pos) = parse_string(bts, pos)
#    (binfo.ns, nl, pos) = parse_string(bts, pos)

  if args.info:
    binfo.print_info()
  return binfo

def register_block(objs, block_type, block_props):
  if block_type == "FRM50_IDFO":
    obj = FormObject(block_props)
    objs[obj.id] = obj
  elif block_type == "F50T":
    t = FormT(block_props)
    obj = objs[t.parent_id]
    obj.props[t.id] = t 
  elif block_type == "F50N":
    n = FormN(block_props)
    obj = objs[n.parent_id]
    obj.props[n.id] = n 
  elif block_type == "F50B":
    b = FormB(block_props)
    obj = objs[b.parent_id]
    obj.props[b.id] = b 
  elif block_type == "F50O":
    o = FormO(block_props)
    obj = objs[o.parent_id]
    obj.props[o.id] = o 
  elif block_type == "F50P":
    obj = FormProc(block_props)
    parent_obj = objs[obj.parent_id]
    parent_obj.procs[obj.id] = obj
    parent_obj.props[obj.id] = obj
  else:
    pass
#    print(block_type)

def parse_file(lines):
  objs = {}

  current_obj = None
  mode = None
  is_block = False
  block_type = None
  block_props = {}
  is_string = False
  for line in lines:
    line = line.strip()
    if is_string:
      value = value + line
      if value.endswith(">>"):
        value = value[2:-2]
        is_string = False
        block_props[key] = value
      continue
    elif line == "BEGIN":
      is_block = True
      block_props = {}
      continue
    elif line == "END":
      is_block = False
      register_block(objs, block_type, block_props)
    elif line.startswith("DEFINE  "):
      block_type = line.split("  ")[1]
    elif line.startswith("DESCRIBE  "):
      block_type = "IGNORE"
    elif " = " in line:
      cols = line.split(" = ")
      if len(cols) == 2:
        key = cols[0]
        value = cols[1]
      else:
        key = cols[0]
        value = " = ".join(cols[1:])
      if value == "(BLONG)":
        is_string = True
        value = ""
        continue
      elif value.startswith("<<") and value.endswith(">>"):
        value = value[2:-2]
      elif value.startswith("<<") and not value.endswith(">>"):
        is_string = True
        continue
      block_props[key] = value
    else:
      pass


  return objs

def get_path(objs, idx, short = True):
  if short:
    return get_short_path(objs, idx)
  else:
    return get_full_path(objs, idx)

def get_short_path(objs, idx):
  obj = objs[idx]
  path = obj.get_short_name()
  while obj is not None and obj.parent_id is not None:    
    parent_id = obj.parent_id
    if parent_id in objs:
      obj = objs[parent_id]
      name = obj.get_short_name()
      if name != "":
        path = name + "/" + path
    else:
      obj = None
  return path

def get_full_path(objs, idx):
  obj = objs[idx]
  path = obj.get_name()
  while obj is not None and obj.parent_id is not None:    
    parent_id = obj.parent_id
    if parent_id in objs:
      obj = objs[parent_id]
      path = obj.get_name() + "->" + path
    else:
      obj = None
  return path


def get_idx_path(objs, idx):
  obj = objs[idx]
  res = []
  while obj is not None:
    res.insert(0, obj.id)
    parent_id = obj.parent_id
    if parent_id is None:
      break
    obj = objs[parent_id]
  return res

def print_results(f, objs):
  first = 0
  for idx in objs:
    obj = objs[idx]
#    print("%d: %s" % (obj.o_type, len(obj.procs)))
    if len(obj.procs) > 0:
      first = first + 1

    if first > 0 and len(obj.procs) > 0: 
      print ("--", file=f)
      print ("-- OBJID: %d" % (obj.id), file=f)
      print ("-- %s" % get_path(objs,idx), file=f)
      print ("--", file=f)
      if len(obj.procs) > 0:
        for key in obj.procs.keys():
          p = obj.procs[key]
          binfo = p.value
          binfo.print_code(f)
        print ("", file=f) 

class TreeNode:
  def __init__(self, objs, idx):
    obj = objs[idx]
    self.id = obj.id
    self.name = obj.get_short_name()
    self.parent_id = obj.parent_id
    self.o_type = obj_types.get(obj.o_type, obj.o_type)
    self.ooi = obj.ooi
    self.opn = obj.opn
    self.cnt = obj.cnt
    self.props = obj.merge_props()
    self.children = {}
    c = [i for i in objs if objs[i].parent_id==idx]
    for cidx in c:
      self.children[cidx] = TreeNode(objs, cidx)
  def __repr__(self):
    return json.dumps(self.__dict__, sort_keys=True, indent=2)

def print_tree(f, objs):
  tree = TreeNode(objs, 1)
  json.dump(tree, f, default=lambda o: o.__dict__, indent=2, ensure_ascii=False)
#  for idx in objs:
#    print("%s" % get_path(objs, idx, False), file=f)
#    print(objs[idx].props, file = f)

parser = argparse.ArgumentParser()
parser.add_argument('--fmt',     required=True,  help="Source fmt file")
parser.add_argument('--code',    required=True,  help="Output code to")
parser.add_argument('--info',    required=False, type=bool, default=False, help="Print blong info")
parser.add_argument('--tree',    required=False, help="Output structure")
parser.add_argument('--nonames', required=False, default=False, action='store_true', help="Don\'t show property names")
args = parser.parse_args()


if args.nonames:
  for i in range(0, 800):
    prop_names[i] = "%04x" %(i)

f = open(args.fmt, encoding="windows-1251")
lines = f.readlines()
f.close()

objs = parse_file(lines)

if args.code is not None:
  f = open(args.code, "wt")
  print_results(f, objs)
  f.close
if args.tree is not None:
  f = open(args.tree, "wt")
  print_tree(f, objs)
  f.close



