#!/usr/bin/python3

import sys

import argparse
import struct

class FmbSection:
  MAGIC0 = 0xffff
  MAGIC1_RAW = 0xffd4
  MAGIC1_MAG = 0xffff
  MAGIC3_3814 = 0x3814
  MAGIC4_a816 = 0xa816
  def __init__(self, pos, magic0, magic1, magic2, magic3, magic4, magic5, bts):
    self.pos = pos
    self.magic0 = magic0
    self.magic1 = magic1
    self.magic2 = magic2
    self.magic3 = magic3
    self.magic4 = magic4
    self.magic5 = magic5
    self.bts = bts
  def __repr__(self):
    return "FormSect(%08x %04x %04x)" % (self.pos,self.magic0,self.magic1)


class FormProp:
  def __init__(self, id, sect, val, val_obj = None, val_str = None):
    self.id = id
    self.sect = sect
    self.val = val
    self.val_obj = val_obj
    self.val_str = val_str

class FormObj:
  def __init__(self):
    self.name = ""
    self.name_sect = 0
    self.name_ref = 0
    self.id = 0
    self.o_type = 0
    self.props = []
    self.pos = 0
    self.ooi_sect = 0
    self.ooi_ref = 0
    self.opn = 0
  def __repr__(self):
    return "FormObj(%08x)" % self.id
  def dump(self):
    pos = self.pos
    if self.name is not None and len(self.name) > 0:
      s = " // %s" % (self.name) 
    else:
      s = ""
    print ("%04x {%04x}<%04x>%s" % (pos, self.name_sect, self.name_ref,s))
    pos = pos + 4
    print ("%04x [oid]<%08x>" % (pos, self.id))
    pos = pos + 4
    print ("%04x [oot]<%04x>" % (pos, self.o_type))
    pos = pos + 2
    print ("%04x [ooi]{%04x}<%04x>" % (pos, self.ooi_sect, self.ooi_ref))
    pos = pos + 4
    print ("%04x [cnt]<%04x>" % (pos, self.cnt))
    pos = pos + 2
    for prop in self.props:
      if prop.val_str is not None and len(prop.val_str)>0:
        s = " // %s" % (prop.val_str)
      elif prop.val_obj is not None:
        s = " // OBJ<%08x>" % (prop.val_obj.id)
      else:
        s = ""
      print ("%04x [%04x]{%04x}<%04x>%s" % (pos, prop.id, prop.sect, prop.val, s))
      pos = pos + 6
    print ("%04x [opn]<%04x>" % (pos, self.opn))
    pos = pos + 2

def dump_ffff(fmb, base_pos, start_pos, end_pos):
  pos = start_pos
  last_ff = start_pos

  while pos < end_pos:
    ff = struct.unpack(">HH", fmb[pos:pos+4])
    if ff[0] != 0xffff or ff[1] !=0xffff:
#    print ("%04x%04x" % ff)
      pos = pos + 4
      continue
    delta = pos - last_ff
    sz = struct.unpack(">H", fmb[last_ff+4:last_ff+6]) 
    (magic3, magic4) = struct.unpack(">HH", fmb[last_ff+6:last_ff+10])
    if magic3 == FmbSection.MAGIC3_3814 and magic4 == FmbSection.MAGIC4_a816:
      dump_raw(fmb[last_ff+20:last_ff+delta], base_pos + last_ff + 20, 0, delta-20)
    else:
      res = struct.unpack(">" +("B"*(delta-6)), fmb[last_ff+6:last_ff+delta])
      print("! %04x-%04x: ffffffff" % (base_pos + last_ff,base_pos + pos), "%04x" % sz, ("%02x" *(delta-6))  % res)
    last_ff = pos
    pos = pos + 4

def dump_raw(fmb, base_pos, start_pos, end_pos):
    res1 = struct.unpack(">HHHH", fmb[start_pos: start_pos+8])
    l = end_pos - start_pos
    (name_sect,name_ref,oid,oot) = struct.unpack(">HHIH", fmb[start_pos:start_pos+10])
    if name_sect == 0 and name_ref == 0x42:
      print ("PL/SQL")
      res = struct.unpack(">"+("B" * l), fmb[start_pos:end_pos])
      print("! %04x-%04x: ffffffd4" % (base_pos-8, base_pos + 0xfff-16), "%04x %04x %04x %04x" % res1, ("%02x"*l) % res)
    elif oid == 0x1 and oot == 0x16:
      res = struct.unpack(">"+("B" * l), fmb[start_pos:end_pos])
      print("! %04x-%04x: ffffffd4" % (base_pos-16, base_pos + 0xfff-16), "%04x %04x %04x %04x" % res1, ("%02x"*l) % res)
      print ("BINGO!")
      print("! %04x-%04x: ffffffd4" % (base_pos-16, base_pos + 0xfff-16), "%04x %04x %04x %04x" % res1)
      objs = []
      (obj, pos) = parse_props(objs, bytes(fmb[start_pos:]), 0)
      for obj in objs:
        obj.dump()
        print("")
    else:
      res = struct.unpack(">"+("B" * l), fmb[start_pos:end_pos])
      print("! %04x-%04x: ffffffd4" % (base_pos-8, base_pos + 0xfff-16), "%04x %04x %04x %04x" % res1, ("%02x"*l) % res)
  
def parse_sections(fmb):
  sections = []
  pos = 0x3000
  while pos < len(fmb):
    res = struct.unpack(">IHHHHHH", fmb[pos:pos+16])
    if pos == 0x4000:
      print (["%04x"%x for x in res])
    section = FmbSection(res[0], res[1], res[2], res[3], res[4], res[5], res[6], fmb[pos+16:pos+0x1000])
    if len(sections)>0 and sections[-1].magic1 == FmbSection.MAGIC1_RAW and section.magic1 == FmbSection.MAGIC1_RAW:
      print ("%08x" %pos, ("%04x " * 4) % (struct.unpack(">HHHH", section.bts[8:16])))
      sections[-1].bts += section.bts
    else:
      sections.append(section)
    pos = pos + 0x1000
  return sections

def dump_header(fmb):
  header = struct.unpack(">IIIIIIIIIIII", fmb[:48])
  print (("%08x " * 4) % header[0:4])

  cnt = header[4]
  size = header[5]
  print (("%08x " * 6) % header[6:12])

  print ("cnt = %d (0x%08x)" % (cnt,cnt))
  print ("size = %d (0x%08x)" % (size,size))
  return (cnt,size)

def parse_string(fmb, pos):
  res = []
  while True:
    if pos > len(fmb)-4:
      print ("!!! %04x" % pos)
      return None
    b = struct.unpack(">BBBB", fmb[pos: pos+4])
    if b[0] == 0 and b[1] == 0 and b[2] == 0 and b[3] == 0:
      break
    res.extend(b)
    pos = pos + 4

  while len(res) > 0 and res[-1] == 0:
    res.pop()

  return str(bytes(res), "windows-1251")
  


def parse_props(objs, fmb, start_pos):
  print ("%04x vs %04x" % (start_pos, len(fmb)))
  pos = start_pos
#  fmb = bytes.fromhex(sample2)
  obj = FormObj()
  objs.append(obj)
  obj.pos = pos
  (obj.name_sect, obj.name_ref) = struct.unpack(">HH",fmb[pos:pos+4])
  if obj.name_sect == 0:
    obj.name = ""
  elif obj.name_sect == 1:
    obj.name = parse_string(fmb, obj.name_ref)
  else:
    obj.name = "{%04x}<%04x>" %(obj.name_sect, obj.name_ref)

  pos = pos + 4
  obj.id = struct.unpack(">I", fmb[pos:pos+4])[0]
  pos = pos + 4
  obj.o_type = struct.unpack(">H", fmb[pos:pos+2])[0]
  pos = pos + 2
  (obj.ooi_sect, obj.ooi_ref) = struct.unpack(">HH",fmb[pos:pos+4])
  pos = pos + 4
  obj.cnt = struct.unpack(">H", fmb[pos:pos+2])[0]
  pos = pos +2
#  obj.dump()
  for i in range(0, obj.cnt):
    if pos > len(fmb)-8:
      break
    (prop_id, prop_sect, prop_val) = struct.unpack(">HHH",fmb[pos:pos+6])
    prop_obj = None
    prop_str = None
    print("%04x [%04x]{%04x}<%04x>" %(pos, prop_id, prop_sect,prop_val) ) 
    
    if prop_sect > 0 and prop_sect < 0x0100:
      shift = prop_val + (prop_sect -1) * 0xff00
      print("{%04x}<%04x> => %08x" % (prop_sect, prop_val, shift))
      if shift > 0 and shift < len(fmb)-8:
        (rb1,rb2,rh1,ri1) = struct.unpack(">BBHI", fmb[shift:shift+8])
        print("%02x%02x %04x %08x" % (rb1,rb2, rh1,ri1))
        valid_ref = True
    else:
      valid_ref = False

    if valid_ref and (rb1 == 0 and rb2 == 0 and rh1 == 0 and ri1 == 0):
      prop_str = ""
    elif valid_ref and  (rb1 != 0): # STR
#      print ("String for {%04x}<%04x>" %(prop_sect, prop_val))
      prop_str = parse_string(fmb, shift)
#      print ("String found for {%04x}<%04x> %s" %(prop_sect, prop_val, prop_str))
    elif valid_ref and rb1 == 0 and ri1 != 0: # OBJ
#      print ("Recursive for {%04x}<%04x>" %(prop_sect, prop_val))
      (prop_obj, new_pos) = parse_props(objs, fmb, shift)

    prop = FormProp(prop_id, prop_sect, prop_val, prop_obj, prop_str)
    obj.props.append(prop)
    pos = pos + 6
  obj.opn = struct.unpack(">H", fmb[pos:pos+2])[0]
#  obj.dump()
  pos = pos + 2


  return (obj,pos)

parser = argparse.ArgumentParser()
parser.add_argument('--fmb', required=True, help="Source fmb file")
args = parser.parse_args()


f = open(args.fmb, mode = "rb")
fmb = f.read()
f.close()

sections = parse_sections(fmb)
for section in sections:
  print (section)
print ("%d" % len(fmb))

(cnt,size) = dump_header(fmb)

dump_ffff(fmb, 0, 0x200, 0x1000)
dump_ffff(fmb, 0, 0x1000, 0x2000)
dump_ffff(fmb, 0, 0x2000, 0x3000)

for section in sections:
  if section.magic1 == FmbSection.MAGIC1_MAG:
    dump_ffff(section.bts, section.pos+8, 8, len(section.bts))
  elif section.magic1 == FmbSection.MAGIC1_RAW:
    dump_raw(section.bts, section.pos+8, 0, len(section.bts))



